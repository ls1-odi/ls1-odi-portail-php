<!-- Le nom de l'UE -->
<div class="UE">Outils pour le développement informatique</div>
<div class="bcc">BCC 1 : 1 Appréhender les approches disciplinaires pour cerner leurs spécificités et leurs complémentarités</div>

<h3> Pré-requis </h3>
Aucun pré-requis disciplinaire pour cette UE, il est néanmoins nécessaire de savoir se servir d'un ordinateur. 

<!-- Volume horaire et organisation de l'UE C, CTD, TD, TP -->
<h3> Organisation  </h3>

<p>
Cette unité obligatoire se déroule au S1 de la licence MI.
</p>
<p>
Volume horaire : 1h30 de TDo par semaine, pendant 12 semaines.</p>

<!-- p>
Cette UE constitue un pré-requis  de l'<a href="http://portail.fil.univ-lille1.fr/????????">UE XXX</a>.
</p -->


<!-- h3> Crédits </h3>

<strong>3 ECTS</strong -->


<!-- le ou les responsable(s) -->
<h3> Responsables </h3>

<p>
Alexandre Sedoglavic, bât M3 extension, bureau XYZ. <a href="mailto:alexandre.sedoglavic AT univ-lille.fr?subject[ODI]">Envoyer un courriel.</a>
</p>

<p>
Cristian Versari, bât M3 extension, bureau 218. <a href="mailto:alexandre.sedoglavic AT univ-lille.fr?subject[ODI]">Envoyer un courriel.</a>
</p>


<!-- signature -->
<div class="signature">
   <!-- Cristian Versari --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
